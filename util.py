# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

'''Baserock system-test utility functions.'''

import cliapp
import contextlib
import copy
import os
import sys
import urlparse

import config


def run_morph(args, **kwargs):
    '''Run Morph on the current machine.

    This is not quite compatible with the run_morph() shell function inside
    Morph's Yarn's because these tests require connectivity to an actual Trove
    and an actual VM host and therefore it's quite a different setup to the
    self-contained Morph test suite.

    '''
    morph_command = copy.copy(config.MORPH_COMMAND)
    if isinstance(morph_command, str):
        if ' ' in morph_command:
            morph_command = morph_command.split(' ')
        else:
            morph_command = [morph_command]

    if config.log_dir is not None:
        morph_command += ['--log', os.path.join(config.log_dir, 'morph.log')]

    cmd = morph_command + args

    if config.VERBOSE:
        print ' '.join(cmd)
        if 'stdout' not in kwargs:
            kwargs['stdout'] = sys.stdout
        if 'stderr' not in kwargs:
            kwargs['stderr'] = sys.stdout
    return cliapp.runcmd(cmd, **kwargs)


def run_git(args, **kwargs):
    return cliapp.runcmd(['git'] + args, **kwargs)


def remote_runcmd(url, command, **kwargs):
    '''
    Execute a command on machine 'url'.

    Command must be a list of arguments, not a single string.

    FIXME: perhaps this functionality should be merged into cliapp.ssh_runcmd()
    so that we can use that instead.
    '''
    if config.VERBOSE:
        print "%s: %s" % (url, ' '.join(command))
    url = urlparse.urlsplit(url)
    if url[0] in ['ssh', 'kvm+ssh']:
        ssh_host = url[1]

        ssh_cmd = ['ssh']

        # The identity of the newly-created test machine will never be in
        # '~/.ssh/known_hosts'; this switch avoids seeing the 'do you want to
        # connect' prompt that SSH would normally present in this situation.
        ssh_cmd.extend(['-o', 'StrictHostKeyChecking=no'])

        return cliapp.runcmd(ssh_cmd + [ssh_host, ' '.join(command)], **kwargs)
    else:
        raise NotImplementedError("Remote machine must be an ssh:// URL")


def read_file(file_path):
    with open(file_path, 'r') as f:
        return f.read()


def write_file(file_path, text):
    with open(file_path, 'w') as f:
        f.write(text)


@contextlib.contextmanager
def set_directory(path):
    '''Context manager to set current working directory of a script.'''
    old_path = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(old_path)
