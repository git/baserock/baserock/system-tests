# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

'''Baserock system-test configuration.'''

import os
import socket

# The test host must have passwordless access to this machine. The tests set
# set StrictHostKeyChecking=no for SSH connections so it does not need to be in
# '~/.ssh/known_hosts'.
DEPLOY_URL = 'kvm+ssh://root@extractor.dyn.ducie.codethink.co.uk/'
DEPLOY_PATH = '/var/lib/libvirt/images/'

# This is the hostname and VM name of the machine that gets deployed.
DEPLOY_NAME = 'brtests-%s' % (socket.gethostname())

# Seconds to wait for machine to appear on network before assuming it didn't
# boot or connect to network successfully.
BOOT_TIMEOUT=20

# Command to run for branch and merge commands, and distbuilding. You might
# want to use a custom version of Morph if you're developing a Morph feature.
MORPH_COMMAND = 'morph'

# This is a wart. Morph should handle this itself (ideally by having one
# distbuild controller able to divide jobs among workers of many architectures.
DISTBUILD_INITIATORS = {
    'armv7lhf': '10.24.1.134',
    'x86_32': '10.24.1.136',
    'x86_64': '10.24.1.164',
}

BUILD_TEMPDIR = '/src/tmp'

BRANCH = 'master'

# For debugging. FIXME: would be better if cliapp's logging mechanism supported
# logging to stdout, but .... :(
VERBOSE = True

# Set by the test runner after initialising the fixture directory.
log_dir = None
